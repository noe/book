# GitLab Issues

GitLab issues are a somewhat formal way to communicate with fellow Redox devs, but a little less quick and convenient than the chat. Issues are a good way to discuss specific topics, but if you want a quick response, using the chat is probably better.

If you haven't requested to join the chat yet, you should (if at all interested in contributing)!

#### Logging Into GitLab With 2FA(Dual-Factor Authentication)

##### Requirements Before Logging Into GitLab

Before logging-in, you'll need:
 - your web browser open at https://gitlab.redox-os.org/redox-os/
 - your phone
 - your 2FA App installed on your phone.
 - to add https://gitlab.redox-os.org/redox-os/ as a site in your 2FA App.  Once added and the site listed, underneith you'll see 2 sets of 3 digits, 6 digits in all. i.e. **258 687**. That's the 2FA Verification Code.  It changes every so often around every minute.

##### Available 2FA Apps for Android
 
 On Google Android, you may use:
 - Google Authenticator https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en_CA&gl=US 
 - **Aegis** Authenticator(open-source) available not only on playstore https://play.google.com/store/apps/details?id=com.beemdevelopment.aegis&hl=en_CA&gl=US but also F-Droid Free and Open Source Software repo as well https://f-droid.org/en/packages/com.beemdevelopment.aegis/

##### Available 2FA Apps for iPhone

 On Apple iphone, you may use:
  - 2stable Authenticator https://9to5mac.com/2021/05/11/2fa-authenticator-app-2stable-iphone-mac-apple-watch-ipad/
  - **Tofu** Authenticator(open-source) https://apps.apple.com/us/app/tofu-authenticator/id1082229305
 
##### Logging-In With An Google Android Phone

Here are the steps:
 - From your computer web browser, open https://gitlab.redox-os.org/redox-os/
 - Click the SignIn button
 - Enter your username/email
 - Enter your password
 - Click the Submit button
 - Finally you will be prompted for a 2FA verification code from your phone. Go to your Android phone, go to Google/**Aegis** Authenticator, find the site gitlab redox and underneith those 6 digits in looking something like **258 687** that's your 2FA code.  Enter those 6 digits into the prompt on your computer.  Click Verify.  Done.  You're logged into Gitlab.
 
##### Logging-In With An Apple iPhone

Here are the steps:
 - From your computer web browser, open https://gitlab.redox-os.org/redox-os/
 - Click the SignIn button
 - Enter your username/email
 - Enter your password
 - Click the Submit button
 - Finally you will be prompted for a 2FA verification code from your phone. Go to your iPhone, go to 2stable/**Tofu** Authenticator, find the site gitlab redox and underneith those 6 digits in looking something like **258 687** that's your 2FA code.  Enter those 6 digits into the prompt on your computer.  Click Verify.  Done.  You're logged into Gitlab.
